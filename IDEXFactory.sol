interface IDEXFactory {
    function createPair(address tokenA, address tokenB)
        external
        returns (address pair);
}